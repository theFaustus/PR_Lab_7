package org.evil.faust.utils;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.evil.faust.utils.UdpServer.connectedUsers;

/**
 * Created by Faust on 4/11/2017.
 */
public class ChatHelper {

    public static void sendMsgLn(String msg, InetAddress address, int port, DatagramSocket clientSocket) {
        try {
            DatagramPacket datagramPacket = new DatagramPacket((msg + "\n").getBytes(), msg.length(), address, port);
            clientSocket.send(datagramPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendPrivateMessage(String command, String senderUsername) {
        Pattern pattern = Pattern.compile("(msg) ([^\"]*) (\"[^\"]*\")");
        Matcher matcher = pattern.matcher(command);
        String message = "";
        String recipient = "";
        while (matcher.find()) {
            recipient = matcher.group(2);
            message = matcher.group(3);
        }
        for (Map.Entry entry : connectedUsers.entrySet()) {
            if (entry.getKey().equals(recipient))
                sendMsgLn((senderUsername + " says : " + message + "\n#END#"), ((UserDetails) entry.getValue()).getAddr(), ((UserDetails) entry.getValue()).getPort(), ((UserDetails) entry.getValue()).getSocket());

        }
    }

    public static void sendGroupMessage(String command, String senderUsername, Map<String, UserDetails> connectedUsers) {
        Pattern pattern = Pattern.compile("(msg) (group) ([^\"]*) (\"[^\"]*\")");
        Matcher matcher = pattern.matcher(command);
        String message = "";
        String groupName = "";
        while (matcher.find()) {
            groupName = matcher.group(3);
            message = matcher.group(4);
        }
        for (Map.Entry entry : connectedUsers.entrySet()) {
            if (!entry.getKey().equals(senderUsername))
                sendMsgLn((groupName + " > " + senderUsername + " says : " + message + "\n#END#"), ((UserDetails) entry.getValue()).getAddr(), ((UserDetails) entry.getValue()).getPort(), ((UserDetails) entry.getValue()).getSocket());

        }
    }

    public static void sendPublicMessage(String command, String senderUsername) {
        Pattern pattern = Pattern.compile("(msg) (\"[^\"]*\")");
        Matcher matcher = pattern.matcher(command);
        String message = "";
        while (matcher.find()) {
            message = matcher.group(2);
        }
        for (Map.Entry entry : connectedUsers.entrySet()) {
            if (!entry.getKey().equals(senderUsername))
                sendMsgLn((senderUsername + " says : " + message + "\n#END#"), ((UserDetails) entry.getValue()).getAddr(), ((UserDetails) entry.getValue()).getPort(), ((UserDetails) entry.getValue()).getSocket());

        }
    }

    public static String getGroupNameFromCreateCommand(String command) {
        Pattern pattern = Pattern.compile("(create) (group) ([^\"]*)");
        Matcher matcher = pattern.matcher(command);
        String groupName = "";
        while (matcher.find()) {
            groupName = matcher.group(3);
        }
        return groupName;
    }

    public static String getGroupNameFromDeleteCommand(String command) {
        Pattern pattern = Pattern.compile("(delete) (group) ([^\"]*)");
        Matcher matcher = pattern.matcher(command);
        String groupName = "";
        while (matcher.find()) {
            groupName = matcher.group(3);
        }
        return groupName;
    }

    public static String getGroupNameFromSendGroupMessageCommand(String command) {
        Pattern pattern = Pattern.compile("(msg) (group) ([^\"]*) (\"[^\"]*\")");
        Matcher matcher = pattern.matcher(command);
        String groupName = "";
        while (matcher.find()) {
            groupName = matcher.group(3);
        }
        return groupName;
    }

    public static String getGroupNameFromAddUserInGroupCommand(String command) {
        Pattern pattern = Pattern.compile("(add) ([^\"]*) (in) ([^\"]*) ([^\"]*)");
        Matcher matcher = pattern.matcher(command);
        String groupName = "";
        while (matcher.find()) {
            groupName = matcher.group(5);
        }
        return groupName;
    }

    public static String getUsernameFromAddUserInGroupCommand(String command) {
        Pattern pattern = Pattern.compile("(add) ([^\"]*) (in) ([^\"]*) ([^\"]*)");
        Matcher matcher = pattern.matcher(command);
        String username = "";
        while (matcher.find()) {
            username = matcher.group(2);
        }
        return username;
    }

    public static boolean isEvaluableSendPrivateMessageCommand(String command) {
        return command.matches("(msg) ([^\"]*) (\"[^\"]*\")");
    }

    public static boolean isEvaluableAddUserInGroupCommand(String command) {
        return command.matches("(add) ([^\"]*) (in) ([^\"]*) ([^\"]*)");
    }

    public static boolean isEvaluableSendPublicMessageCommand(String command) {
        return command.matches("(msg) (\"[^\"]*\")");
    }

    public static boolean isEvaluableSendGroupMessageCommand(String command) {
        return command.matches("(msg) (group) ([^\"]*) (\"[^\"]*\")");
    }

    public static boolean isEvaluableCreateGroupCommand(String command) {
        return command.matches("(create) (group) ([^\"]*)");
    }

    public static boolean isEvaluableDeleteGroupCommand(String command) {
        return command.matches("(delete) (group) ([^\"]*)");
    }
}
