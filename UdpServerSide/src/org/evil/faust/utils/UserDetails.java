package org.evil.faust.utils;

import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by Faust on 5/10/2017.
 */
public class UserDetails {
    private DatagramSocket socket;
    private InetAddress addr;
    private int port;

    UserDetails(InetAddress a, int p, DatagramSocket s){
        addr = a;
        port = p;
        socket = s;
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public void setSocket(DatagramSocket socket) {
        this.socket = socket;
    }

    public InetAddress getAddr() {
        return addr;
    }

    public void setAddr(InetAddress addr) {
        this.addr = addr;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}