package org.evil.faust.utils;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Faust on 5/9/2017.
 */


public class UdpServer {
    private final int PORT = 8888;
    private DatagramSocket serverSocket;
    private static int clientId = 1;
    private DatagramPacket datagramPacket, out;
    private ExecutorService executor = Executors.newFixedThreadPool(10);
    private byte[] buffer;
    private HashMap<String, UserDetails> users;
    private Random random = new Random();
    static Map<String, UserDetails> connectedUsers = new HashMap<>();


    public UdpServer() {
        try {
            serverSocket = new DatagramSocket(PORT);
            System.out.println("Server is up.");
            System.out.println("Goodies Server 2.0");
            System.out.println("Listening on port " + this.PORT);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        users = new HashMap<>();
    }

    private void sendMsg(InetAddress addr, int port, String msg) {
        try {
            out = new DatagramPacket(msg.getBytes(), msg.length(), addr, port);
            serverSocket.send(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleClients() {
        try {
            String msgIn, msgOut = "";
            while (true) {
                buffer = new byte[65536];
                datagramPacket = new DatagramPacket(buffer, buffer.length);
                serverSocket.receive(datagramPacket);
                InetAddress clientAddress = datagramPacket.getAddress();
                int clientPort = datagramPacket.getPort();

                msgIn = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                System.out.println(msgIn);

                if (msgIn.equalsIgnoreCase("hello")) {
                    sendMsg(clientAddress, clientPort, "Accepted connection.\n#END#");
                    System.out.println("Connection established!");
                    DatagramSocket workerSocket = new DatagramSocket();
                    this.executor.execute(new ClientHandler(workerSocket, datagramPacket, clientId++));
                } else {
                    sendMsg(clientAddress, clientPort, "Say HELLO to welcome the server\n#END#");
                }
            }
        } catch (
                IOException e)

        {
            e.printStackTrace();
        } finally

        {
            System.out.println("Closing connection.");
            serverSocket.close();
        }
    }

}