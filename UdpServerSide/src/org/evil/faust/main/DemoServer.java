package org.evil.faust.main;

import org.evil.faust.utils.UdpServer;

/**
 * Created by Faust on 5/9/2017.
 */
public class DemoServer {
    public static void main(String[] args) {
        UdpServer udpServer = new UdpServer();
        udpServer.handleClients();
    }
}
