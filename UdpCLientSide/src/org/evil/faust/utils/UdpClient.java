package org.evil.faust.utils;

import java.io.IOException;
import java.net.*;

/**
 * Created by Faust on 5/9/2017.
 */
public class UdpClient implements Observer{

    private InetAddress inetAddress;
    private int port = 8888;
    private DatagramSocket sckt;
    private DatagramPacket in, out;
    private byte[] buffer;
    private String command;


    public UdpClient(InetAddress inetAddress){
        this.inetAddress = inetAddress;
    }


    private void sendMsg(String msg) {
        try {
            out = new DatagramPacket(msg.getBytes(), msg.length(), inetAddress, port);
            sckt.send(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        ScannerReader scannerReader = new ScannerReader();
        scannerReader.registerObserver(this);
        scannerReader.setDaemon(true);
        scannerReader.start();

        try {
            System.out.println("Establishing connection... please wait.");
            sckt = new DatagramSocket();
            System.out.println("Connection established on " + inetAddress + ":" + port);
            String reply = "";

            while (true) {
                reply = "";
                while (!sckt.isClosed()) {
                    buffer = new byte[65536];
                    in = new DatagramPacket(buffer, buffer.length);
                    sckt.receive(in);
                    reply = validateOneLine();
                    System.out.println(reply);
                    if(reply.startsWith("Welcome")){
                        port = in.getPort();
                        inetAddress = in.getAddress();
                    }
                    if (reply.equals("#END#"))
                        break;
                }

                if (command.equalsIgnoreCase("bye")) {
                    sendMsg("bye");
                    sckt.close();
                    break;
                }


            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String validateOneLine() throws IOException {
        String reply = new String(in.getData(), 0, in.getLength());
        if (reply.isEmpty())
            return null;
        return reply;
    }

    @Override
    public void consumeCommand(String command) {
        this.command = command;
        sendMsg(command);
    }
}
