package org.evil.faust.main;

import org.evil.faust.utils.UdpClient;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Faust on 5/9/2017.
 */
public class DemoClient {

    public static void main(String[] args) {
        UdpClient udpClient = null;
        try {
            udpClient = new UdpClient(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        udpClient.connect();
    }
}
